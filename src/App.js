import React, { Component } from "react";
import Table from './component/Table';
import { Button } from 'reactstrap';
import './App.css';


class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            persons: '',
            fetch: false
        };
    };
    componentWillMount() {
        fetch('http://178.128.196.163:3000/api/records',{method:'GET'})
            .then(response => response.json())
            .then((data) => {
                return this.setState({ persons: data,  fetch: !this.state.fetch})
            })
    };
    add = text => {
        let data = {data: {name: text}}
        fetch('http://178.128.196.163:3000/api/records', 
                {method: 'PUT',
                    headers: {
                        'Accept': 'application/json, text/plain, */*',
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify(data)
                })
    };

    deleteBlock = i => {
        fetch(`http://178.128.196.163:3000/api/records/${i}`, {method: 'delete'})
            .then(response => response.json())
    };

    updateText = (name,surname, i) => {
        let data = {
            data: {
                name: name,
                surname: surname
            }
        }
        fetch(`http://178.128.196.163:3000/api/records/${i}`, 
                {method: 'post',
                headers: {
                    'Accept': 'application/json, text/plain, */*',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data)
            })
    };
    getFetch =() =>{
        fetch('http://178.128.196.163:3000/api/records',{method:'GET'})
        .then(response => response.json())
        .then((data) => this.setState({ persons: data, fetch: !this.state.fetch }))
    }
    componentDidMount () {
        this.getFetch();
        this.timer = setInterval(() => this.getFetch(), 100);
    }
    componentWillUnmount() {
        this.timer = null;
     }
    eachTask = (item) => {
        return (
            <div>
                <Table key={item._id} index={item._id} name={item.data.name} surname={item.data.surname} update={this.updateText} deleteBlock={this.deleteBlock} />
            </div>
        );
    };
    
    render() {
        if (!this.state.persons.length) return null
        return (
            <div className="container">
               <div >{this.state.persons.map(this.eachTask)}</div>
                <Button className="button-o" outline color="secondary" onClick={this.add.bind(null,'Name')}>Add</Button>
            </div>
        );
    }
}

export default App;
