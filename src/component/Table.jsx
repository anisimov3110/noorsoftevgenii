import React, { Component } from "react";
import { Button, ButtonGroup } from 'reactstrap';
import './Table.css';

class Task extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            edit: false
        };
    }
    edit = () => {
        this.setState({ edit: true });
    };
    remove = () => {
        this.props.deleteBlock(this.props.index)
    };
    save = () => {
        this.props.update(this.refs.newName.value, this.refs.newSurName.value, this.props.index)
        this.setState({ edit: false });
    };
    outside = () => {
        return (
            <div className="container">
                <div className="row">
                    <input className="col-5 input" defaultValue={this.props.name} disabled='disabled'/>
                    <input className="col-5 input" defaultValue={this.props.surname} disabled='disabled'/>
                    <div className="col-2">
                        <ButtonGroup>
                            <Button onClick={this.edit}outline color="secondary">
                                Edit
                            </Button>
                            <Button onClick={this.remove} outline color="secondary">
                                Delete
                            </Button>
                        </ButtonGroup>
                    </div>
                </div>
            </div>
        );
    };
    inside = () => {
        return (
            <div className="container">
               <div className="row">
                   <input className="col-5 input" ref="newName" defaultValue={this.props.name} />
                   <input className="col-5 input" ref="newSurName" defaultValue={this.props.surname} />
                   <Button className="col-10 " size="sm" outline color="secondary" onClick={this.save} >
                       Save
                   </Button>
               </div>
            </div>
        );
    };
    render() {
        if (this.state.edit) {
            return this.inside();
        } else {
            return this.outside();
        }
    }
}

export default Task;